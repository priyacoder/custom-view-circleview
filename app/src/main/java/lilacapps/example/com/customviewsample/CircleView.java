package lilacapps.example.com.customviewsample;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;

/**
 * Created by Vishnu Priya Nallan on 2/28/2018.
 */

public class CircleView extends View {

    private int circleCol, labelCol;
    private String circleText;
    private Paint circlePaint;

    public CircleView(Context context, AttributeSet attributeSet) {
        super(context,attributeSet);
        circlePaint = new Paint();

        TypedArray typedArray = context.obtainStyledAttributes(attributeSet, R.styleable.CircleView, 0, 0);

        try{
            circleText = typedArray.getString(R.styleable.CircleView_circleLabel);
            circleCol = typedArray.getColor(R.styleable.CircleView_circleColor,0);
            labelCol = typedArray.getColor(R.styleable.CircleView_labelColor, 0);
        }
        finally {
            typedArray.recycle();
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {

        super.onDraw(canvas);

        int x = getWidth();
        int y = getMeasuredWidth();
        int viewWidthHalf = this.getMeasuredWidth()/2;
        int viewHeightHalf = this.getMeasuredHeight()/2;

        int radius = 0;
        if (viewWidthHalf > viewHeightHalf) {
            radius = viewHeightHalf - 10;
        } else {
            radius = viewWidthHalf - 10;
        }

        // Paint the circle
        circlePaint.setStyle(Paint.Style.FILL);
        circlePaint.setAntiAlias(true);
        circlePaint.setColor(circleCol);
        canvas.drawCircle(viewWidthHalf, viewHeightHalf, radius, circlePaint);

        // Set text properties
        circlePaint.setColor(labelCol);
        circlePaint.setTextAlign(Paint.Align.CENTER);
        circlePaint.setTextSize(50);
        canvas.drawText(circleText, viewWidthHalf, viewHeightHalf, circlePaint);

    }

    public int getCircleCol() {
        return circleCol;
    }

    public void setCircleCol(int newCircleColor) {
        this.circleCol = newCircleColor;
        invalidate();
    }

    public int getLabelCol() {
        return labelCol;
    }

    public void setLabelCol(int newLabelColor) {
        this.labelCol = newLabelColor;
        invalidate();
    }

    public String getCircleText() {
        return circleText;
    }

    public void setCircleText(String newLabel) {
        this.circleText = newLabel;
        invalidate();
    }
}
