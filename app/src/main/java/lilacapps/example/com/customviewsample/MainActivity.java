package lilacapps.example.com.customviewsample;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    private CircleView customCircleView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        customCircleView = findViewById(R.id.custCircleView);
    }

    public void btnPressed(View view) {
        customCircleView.setCircleCol(Color.GREEN);
        customCircleView.setCircleText("Hello");
        customCircleView.setLabelCol(Color.MAGENTA);
    }
}
